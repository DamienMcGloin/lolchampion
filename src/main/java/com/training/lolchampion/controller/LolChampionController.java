package com.training.lolchampion.controller;

import com.training.lolchampion.entities.LolChampion;
import com.training.lolchampion.service.LolChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lolchampion")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        LOG.debug("Request to create lolChampion [" + lolChampion + "]");
        return this.lolChampionService.save(lolChampion);
    }

    /*
    @DeleteMapping
    public void delete(@PathVariable long id) {
        try {
            lolChampionService.delete(id);
        } catch() {
        }
    }

     */

}
