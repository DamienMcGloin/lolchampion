package com.training.lolchampion.entities;

import javax.persistence.*;

@Entity(name="lolchampions")
public class LolChampion {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="LolChampionID")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="role")
    private String role;

    @Column(name="difficulty")
    private String difficulty;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public String toString() {
        return "LolChampion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", difficulty='" + difficulty + '\'' +
                '}';
    }

}
